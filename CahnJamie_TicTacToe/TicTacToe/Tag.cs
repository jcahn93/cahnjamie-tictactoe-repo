﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe
{
    class Tag
    {
        private int _color;
        private char _symbol;

        public Tag(int color, char symbol)
        {
            _color = color;
            _symbol = symbol;
        }

        public int Color { get => _color; set => _color = value; }
        public char Symbol { get => _symbol; set => _symbol = value; }
    }
}
