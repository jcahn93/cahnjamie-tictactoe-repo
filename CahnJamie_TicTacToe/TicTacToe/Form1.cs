﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Xml;
using System.IO;

namespace TicTacToe
{
    public partial class frmTicTacToe : Form
    {
        // NAME: Jamie Cahn
        // CLASS AND TERM: DVP3 - September 2018
        // PROJECT: Tic Tac Toe

        /* THINGS TO CONSIDER:
            - You must change the project name to conform to the required
              naming convention.
        */


        public frmTicTacToe()
        {
            InitializeComponent();
            buttons();
        }

        private void buttons()
        {
            r1c1button.Tag = 2;
            r1c2button.Tag = 2;
            r1c3button.Tag = 2;
            r2c1button.Tag = 2;
            r2c2button.Tag = 2;
            r2c3button.Tag = 2;
            r3c1button.Tag = 2;
            r3c2button.Tag = 2;
            r3c3button.Tag = 2;
        }
        //toolstrip region that contains
        //exit button, View ToolStrip,
        //Select Toolstrip
        #region ToolStrip


        //variables needed for save/load
        string b1 = null;
        string b2 = null;
        string b3 = null;
        string b4 = null;
        string b5 = null;
        string b6 = null;
        string b7 = null;
        string b8 = null;
        string b9 = null;
        string s1 = null;
        string s2 = null;
        string s3 = null;
        string s4 = null;
        string s5 = null;
        string s6 = null;
        string s7 = null;
        string s8 = null;
        string s9 = null;




        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit(); //close the application
        } //done

        private void blueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (LockPlayer() == true) //if not in current game
            {
                if (redToolStripMenuItem.Checked == true) //check to see if the red button is clicked
                {
                    //switch from red to blue
                    redToolStripMenuItem.Checked = false;
                    blueToolStripMenuItem.Checked = true;

                }
            }
            else //if in a current game
            {
                MessageBox.Show("Cannot change player information in current game.");
            }
        } //done

        private void redToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (LockPlayer() == true) //if not in current game
            {
                if (blueToolStripMenuItem.Checked == true) //check to see if the blue button is clicked
                {
                    //switch from blue to red
                    blueToolStripMenuItem.Checked = false;
                    redToolStripMenuItem.Checked = true;
                }
            }
            else //if in a current game
            {
                MessageBox.Show("Cannot change player information in current game.");
            }
        } //done

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            //clear first row
            r1c1button.Image = null;
            r1c2button.Image = null;
            r1c3button.Image = null;

            //clear second row
            r2c1button.Image = null;
            r2c2button.Image = null;
            r2c3button.Image = null;

            //clear third row
            r3c1button.Image = null;
            r3c2button.Image = null;
            r3c3button.Image = null;

            //reset the view menu
            redToolStripMenuItem.Checked = false;
            blueToolStripMenuItem.Checked = true;

            //reset the select menu
            xToolStripMenuItem.Checked = false;
            oToolStripMenuItem.Checked = false;

            MessageBox.Show("Please select a color to start and an X or O to start.");
        } //done

        private void xToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (LockPlayer() == true) //if not in a current game
            {
                //change the current players turn
                if (oToolStripMenuItem.Checked == true)
                {
                    oToolStripMenuItem.Checked = false;
                    xToolStripMenuItem.Checked = true;
                }
                else
                {
                    xToolStripMenuItem.Checked = true;
                }
            }
            else //if in a current game
            {
                MessageBox.Show("Cannot change player information in current game.");
            }

        } //done

        private void oToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (LockPlayer() == true) //if there is not a current game
            {
                //change the current player
                if (xToolStripMenuItem.Checked == true)
                {
                    xToolStripMenuItem.Checked = false;
                    oToolStripMenuItem.Checked = true;
                }
                else
                {
                    oToolStripMenuItem.Checked = true;
                }
            }
            else //if in a current game
            {
                MessageBox.Show("Cannot change player information in current game.");
            }
        } //done
        private void saveGameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //if 0 = blue
            //if 1 = red
            string b1 = r1c1button.Tag.ToString();
            string b2 = r1c2button.Tag.ToString();
            string b3 = r1c3button.Tag.ToString();
            string b4 = r2c1button.Tag.ToString();
            string b5 = r2c2button.Tag.ToString();
            string b6 = r2c3button.Tag.ToString();
            string b7 = r3c1button.Tag.ToString();
            string b8 = r3c2button.Tag.ToString();
            string b9 = r3c3button.Tag.ToString();
            string s1 = null;
            string s2 = null;
            string s3 = null;
            string s4 = null;
            string s5 = null;
            string s6 = null;
            string s7 = null;
            string s8 = null;
            string s9 = null;

            #region b1
            if (r1c1button.ImageIndex == 1)
            {
                s1 = "x";
            }
            else if (r1c1button.ImageIndex == 0)
            {
                s1 = "o";
            }
            else
            {
                s1 = "";
            }
            #endregion b1

            #region b2
            if (r1c2button.ImageIndex == 1)
            {
                s2 = "x";
            }
            else if (r1c2button.ImageIndex == 0)
            {
                s2 = "o";
            }
            else
            {
                s2 = "";
            }
            #endregion b2

            #region b3

            if (r1c3button.ImageIndex == 1)
            {
                s3 = "x";
            }
            else if (r1c3button.ImageIndex == 0)
            {
                s3 = "o";
            }
            else
            {
                s3 = "";
            }
            #endregion b3

            #region b4

            if (r2c1button.ImageIndex == 1)
            {
                s4 = "x";
            }
            else if (r2c1button.ImageIndex == 0)
            {
                s4 = "o";
            }
            else
            {
                s4 = "";
            }

            #endregion b4

            #region b5

            if (r2c2button.ImageIndex == 1)
            {
                s5 = "x";
            }
            else if (r2c2button.ImageIndex == 0)
            {
                s5 = "o";
            }
            else
            {
                s5 = "";
            }

            #endregion b5

            #region b6

            if (r2c3button.ImageIndex == 1)
            {
                s6 = "x";
            }
            else if (r2c3button.ImageIndex == 0)
            {
                s6 = "o";
            }
            else
            {
                s6 = "";
            }

            #endregion b6

            #region b7

            if (r3c1button.ImageIndex == 1)
            {
                s7 = "x";
            }
            else if (r3c1button.ImageIndex == 0)
            {
                s7 = "o";
            }
            else
            {
                s7 = "";
            }

            #endregion b7

            #region b8

            if (r3c2button.ImageIndex == 1)
            {
                s8 = "x";
            }
            else if (r3c2button.ImageIndex == 0)
            {
                s8 = "o";
            }
            else
            {
                s8 = "";
            }

            #endregion b8
            
            #region b9

            if (r3c3button.ImageIndex == 1)
            {
                s9 = "x";
            }
            else if (r3c3button.ImageIndex == 0)
            {
                s9 = "o";
            }
            else
            {
                s9 = "";
            }

            #endregion b9

            OpenFileDialog ofd = new OpenFileDialog();
            ofd.DefaultExt = "xml";
            ofd.Filter = "XML Files | *.xml";
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                using (StreamWriter sw = new StreamWriter(ofd.FileName))
                {
                    //xml data writer
                    XmlWriter xmlWriter = XmlWriter.Create("data.xml");

                    //start writing the document
                    xmlWriter.WriteStartDocument();
                    xmlWriter.WriteStartElement("Buttons");
                    //button1
                    xmlWriter.WriteStartElement("Button1");
                    xmlWriter.WriteAttributeString("Color", b1);
                    xmlWriter.WriteAttributeString("Symbol", s1);
                    xmlWriter.WriteEndElement();
                    //button2
                    xmlWriter.WriteStartElement("Button2");
                    xmlWriter.WriteAttributeString("Color", b2);
                    xmlWriter.WriteAttributeString("Symbol", s2);
                    xmlWriter.WriteEndElement();
                    //button3
                    xmlWriter.WriteStartElement("Button3");
                    xmlWriter.WriteAttributeString("Color", b3);
                    xmlWriter.WriteAttributeString("Symbol", s3);
                    xmlWriter.WriteEndElement();
                    //button4
                    xmlWriter.WriteStartElement("Button4");
                    xmlWriter.WriteAttributeString("Color", b4);
                    xmlWriter.WriteAttributeString("Symbol", s4);
                    xmlWriter.WriteEndElement();
                    //button5
                    xmlWriter.WriteStartElement("Button5");
                    xmlWriter.WriteAttributeString("Color", b5);
                    xmlWriter.WriteAttributeString("Symbol", s5);
                    xmlWriter.WriteEndElement();
                    //button6
                    xmlWriter.WriteStartElement("Button6");
                    xmlWriter.WriteAttributeString("Color", b6);
                    xmlWriter.WriteAttributeString("Symbol", s6);
                    xmlWriter.WriteEndElement();
                    //button7
                    xmlWriter.WriteStartElement("Button7");
                    xmlWriter.WriteAttributeString("Color", b7);
                    xmlWriter.WriteAttributeString("Symbol", s7);
                    xmlWriter.WriteEndElement();
                    //button8
                    xmlWriter.WriteStartElement("Button8");
                    xmlWriter.WriteAttributeString("Color", b8);
                    xmlWriter.WriteAttributeString("Symbol", s8);
                    xmlWriter.WriteEndElement();
                    //button9
                    xmlWriter.WriteStartElement("Button9");
                    xmlWriter.WriteAttributeString("Color", b9);
                    xmlWriter.WriteAttributeString("Symbol", s9);
                    xmlWriter.WriteEndElement();

                    //close the document
                    xmlWriter.WriteEndDocument();
                    xmlWriter.Close();
                }
            }
        } //done

        private void loadGameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            b1 = null;
            b2 = null;
            b3 = null;
            b4 = null;
            b5 = null;
            b6 = null;
            b7 = null;
            b8 = null;
            b9 = null;
            s1 = null;
            s2 = null;
            s3 = null;
            s4 = null;
            s5 = null;
            s6 = null;
            s7 = null;
            s8 = null;
            s9 = null;
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.DefaultExt = "xml";
            ofd.Filter = "XML Files | *.xml";
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                using (StreamReader sr = new StreamReader(ofd.FileName))
                {
                    XmlReader reader = XmlReader.Create(ofd.FileName);
                    while (reader.Read())
                    {
                        if ((reader.NodeType == XmlNodeType.Element) && reader.Name == "Button1")
                        {
                            if (reader.HasAttributes)
                            {
                                b1 = reader.GetAttribute("Color");
                                s1 = reader.GetAttribute("Symbol");
                            }
                        }
                        if ((reader.NodeType == XmlNodeType.Element) && reader.Name == "Button2")
                        {
                            if (reader.HasAttributes)
                            {
                                b2 = reader.GetAttribute("Color");
                                s2 = reader.GetAttribute("Symbol");
                            }
                        }
                        if ((reader.NodeType == XmlNodeType.Element) && reader.Name == "Button3")
                        {
                            if (reader.HasAttributes)
                            {
                                b3 = reader.GetAttribute("Color");
                                s3 = reader.GetAttribute("Symbol");
                            }
                        }
                        if ((reader.NodeType == XmlNodeType.Element) && reader.Name == "Button4")
                        {
                            if (reader.HasAttributes)
                            {
                                b4 = reader.GetAttribute("Color");
                                s4 = reader.GetAttribute("Symbol");
                            }
                        }
                        if ((reader.NodeType == XmlNodeType.Element) && reader.Name == "Button5")
                        {
                            if (reader.HasAttributes)
                            {
                                b5 = reader.GetAttribute("Color");
                                s5 = reader.GetAttribute("Symbol");
                            }
                        }
                        if ((reader.NodeType == XmlNodeType.Element) && reader.Name == "Button6")
                        {
                            if (reader.HasAttributes)
                            {
                                b6 = reader.GetAttribute("Color");
                                s6 = reader.GetAttribute("Symbol");
                            }
                        }
                        if ((reader.NodeType == XmlNodeType.Element) && reader.Name == "Button7")
                        {
                            if (reader.HasAttributes)
                            {
                                b7 = reader.GetAttribute("Color");
                                s7 = reader.GetAttribute("Symbol");
                            }
                        }
                        if ((reader.NodeType == XmlNodeType.Element) && reader.Name == "Button8")
                        {
                            if (reader.HasAttributes)
                            {
                                b8 = reader.GetAttribute("Color");
                                s8 = reader.GetAttribute("Symbol");
                            }
                        }
                        if ((reader.NodeType == XmlNodeType.Element) && reader.Name == "Button9")
                        {
                            if (reader.HasAttributes)
                            {
                                b9 = reader.GetAttribute("Color");
                                s9 = reader.GetAttribute("Symbol");
                            }
                        }
                    }

                    //the folliowing section of buttons takes the XML data and sets it equal to the buttons
                    #region button1
                    if (b1 == "0")
                    {
                        r1c1button.Tag = b1;
                        if (s1 == "x")
                        {
                            r1c1button.Image = blueImages.Images[1];
                        }
                        else if (s1 == "o")
                        {
                            r1c1button.Image = blueImages.Images[0];
                        }
                        else
                        {
                            r1c1button.Image = null;
                        }
                    }
                    else if (b1 == "1")
                    {
                        r1c1button.Tag = b1;
                        if (s1 == "x")
                        {
                            r1c1button.Image = redImages.Images[1];
                        }
                        else if (s1 == "o")
                        {
                            r1c1button.Image = redImages.Images[0];
                        }
                        else
                        {
                            r1c1button.Image = null;
                        }
                    }
                    else
                    {
                        r1c1button.Tag = "2";
                        r1c1button.Image = null;
                    }
                    #endregion button1

                    #region button2

                    if (b2 == "0")
                    {
                        r1c2button.Tag = b2;
                        if (s2 == "x")
                        {
                            r1c2button.Image = blueImages.Images[1];
                        }
                        else if (s2 == "o")
                        {
                            r1c2button.Image = blueImages.Images[0];
                        }
                        else
                        {
                            r1c2button.Image = null;
                        }
                    }
                    else if (b2 == "1")
                    {
                        r1c2button.Tag = b2;
                        if (s2 == "x")
                        {
                            r1c2button.Image = redImages.Images[1];
                        }
                        else if (s2 == "o")
                        {
                            r1c2button.Image = redImages.Images[0];
                        }
                        else
                        {
                            r1c2button.Image = null;
                        }
                    }
                    else
                    {
                        r1c2button.Tag = "2";
                        r1c2button.Image = null;
                    }

                    #endregion button2

                    #region button3

                    if (b3 == "0")
                    {
                        r1c3button.Tag = b3;
                        if (s3 == "x")
                        {
                            r1c3button.Image = blueImages.Images[1];
                        }
                        else if (s3 == "o")
                        {
                            r1c3button.Image = blueImages.Images[0];
                        }
                        else
                        {
                            r1c3button.Image = null;
                        }
                    }
                    else if (b3 == "1")
                    {
                        r1c3button.Tag = b3;
                        if (s3 == "x")
                        {
                            r1c3button.Image = redImages.Images[1];
                        }
                        else if (s3 == "o")
                        {
                            r1c3button.Image = redImages.Images[0];
                        }
                        else
                        {
                            r1c3button.Image = null;
                        }
                    }
                    else
                    {
                        r1c3button.Tag = "2";
                        r1c3button.Image = null;
                    }

                    #endregion button3

                    #region button4
                    if (b4 == "0")
                    {
                        r2c1button.Tag = b4;
                        if (s4 == "x")
                        {
                            r2c1button.Image = blueImages.Images[1];
                        }
                        else if (s4 == "o")
                        {
                            r2c1button.Image = blueImages.Images[0];
                        }
                        else
                        {
                            r2c1button.Image = null;
                        }
                    }
                    else if (b4 == "1")
                    {
                        r2c1button.Tag = b4;
                        if (s4 == "x")
                        {
                            r2c1button.Image = redImages.Images[1];
                        }
                        else if (s4 == "o")
                        {
                            r2c1button.Image = redImages.Images[0];
                        }
                        else
                        {
                            r2c1button.Image = null;
                        }
                    }
                    else
                    {
                        r2c1button.Tag = "2";
                        r2c1button.Image = null;
                    }
                    #endregion button4

                    #region button5
                    if (b5 == "0")
                    {
                        r2c2button.Tag = b5;
                        if (s5 == "x")
                        {
                            r2c2button.Image = blueImages.Images[1];
                        }
                        else if (s5 == "o")
                        {
                            r2c2button.Image = blueImages.Images[0];
                        }
                        else
                        {
                            r2c2button.Image = null;
                        }
                    }
                    else if (b5 == "1")
                    {
                        r2c2button.Tag = b5;
                        if (s5 == "o")
                        {
                            r2c2button.Image = redImages.Images[1];
                        }
                        else if (s5 == "x")
                        {
                            r2c2button.Image = redImages.Images[0];
                        }
                        else
                        {
                            r2c2button.Image = null;
                        }
                    }
                    else
                    {
                        r2c2button.Tag = "2";
                        r2c2button.Image = null;
                    }

                    #endregion button5

                    #region button6
                    if (b6 == "0")
                    {
                        r2c3button.Tag = b6;
                        if (s6 == "x")
                        {
                            r2c3button.Image = blueImages.Images[1];
                        }
                        else if (s6 == "o")
                        {
                            r2c3button.Image = blueImages.Images[0];
                        }
                        else
                        {
                            r2c3button.Image = null;
                        }
                    }
                    else if (b6 == "1")
                    {
                        r2c3button.Tag = b6;
                        if (s6 == "x")
                        {
                            r2c3button.Image = redImages.Images[1];
                        }
                        else if (s6 == "o")
                        {
                            r2c3button.Image = redImages.Images[0];
                        }
                        else
                        {
                            r2c3button.Image = null;
                        }
                    }
                    else
                    {
                        r2c3button.Tag = "2";
                        r2c3button.Image = null;
                    }
                    #endregion button6

                    #region button7
                    if (b7 == "0")
                    {
                        r3c1button.Tag = b7;
                        if (s7 == "x")
                        {
                            r3c1button.Image = blueImages.Images[1];
                        }
                        else if (s7 == "o")
                        {
                            r3c1button.Image = blueImages.Images[0];
                        }
                        else
                        {
                            r3c1button.Image = null;
                        }
                    }
                    else if (b7 == "1")
                    {
                        r3c1button.Tag = b7;
                        if (s7 == "x")
                        {
                            r3c1button.Image = redImages.Images[1];
                        }
                        else if (s7 == "o")
                        {
                            r3c1button.Image = redImages.Images[0];
                        }
                        else
                        {
                            r3c1button.Image = null;
                        }
                    }
                    else
                    {
                        r3c1button.Tag = "2";
                        r3c1button.Image = null;
                    }

                    #endregion button7

                    #region button8
                    if (b8 == "0")
                    {
                        r3c2button.Tag = b8;
                        if (s8 == "x")
                        {
                            r3c2button.Image = blueImages.Images[1];
                        }
                        else if (s8 == "o")
                        {
                            r3c2button.Image = blueImages.Images[0];
                        }
                        else
                        {
                            r3c2button.Image = null;
                        }
                    }
                    else if (b8 == "1")
                    {
                        r3c2button.Tag = b8;
                        if (s8 == "x")
                        {
                            r3c2button.Image = redImages.Images[1];
                        }
                        else if (s8 == "o")
                        {
                            r3c2button.Image = redImages.Images[0];
                        }
                        else
                        {
                            r3c2button.Image = null;
                        }
                    }
                    else
                    {
                        r3c2button.Image = null;
                    }
                    #endregion button8

                    #region button9
                    if (b9 == "0")
                    {
                        r3c3button.Tag = b9;
                        if (s9 == "x")
                        {
                            r3c3button.Image = blueImages.Images[1];
                        }
                        else if (s9 == "o")
                        {
                            r3c3button.Image = blueImages.Images[0];
                        }
                        else
                        {
                            r3c3button.Image = null;
                        }
                    }
                    else if (b9 == "1")
                    {
                        r3c3button.Tag = b9;
                        if (s9 == "x")
                        {
                            r3c3button.Image = redImages.Images[1];
                        }
                        else if (s9 == "o")
                        {
                            r3c3button.Image = redImages.Images[0];
                        }
                        else
                        {
                            r3c3button.Image = null;
                        }
                    }
                    else
                    {
                        r3c3button.Tag = "2";
                        r3c3button.Image = null;
                    }

                    #endregion button9
                }
            }

        }
        #endregion Toolstrip 

        //If the button is a blue = 0
        //if the button is a red = 1


        //ButtonFunctionality region that contains
        //Button to Image Functionality for 9 buttons
        //in the game
        #region ButtonFunctionality

        private void r1c1button_Click(object sender, EventArgs e)
        {
            if (r1c1button.Image == null)
            {
                if (blueToolStripMenuItem.Checked == true)
                {
                    //blue turn
                    if (xToolStripMenuItem.Checked == true)
                    {
                        //if blue user is x
                        r1c1button.Image = blueImages.Images[1];
                        r1c1button.Tag = 0;
                    }
                    else if (oToolStripMenuItem.Checked == true)
                    {
                        //if blue user is o
                        r1c1button.Image = blueImages.Images[0];
                        r1c1button.Tag = 0;
                    }
                    else
                    {
                        //msg box for issues
                        MessageBox.Show("Please select a X or O from the Select Menu.");
                    }
                }
                else if (redToolStripMenuItem.Checked == true)
                {
                    //red turn
                    if (xToolStripMenuItem.Checked == true)
                    {
                        //if user is x
                        r1c1button.Image = redImages.Images[1];
                        r1c1button.Tag = 1;
                    }
                    else if (oToolStripMenuItem.Checked == true)
                    {
                        //if user is o
                        r1c1button.Image = redImages.Images[0];
                        r1c1button.Tag = 1;
                    }
                    else
                    {
                        //msg box for issues
                        MessageBox.Show("Please select a X or O from the Select Menu.");
                    }

                }
                else
                {
                    //msg box for issues
                    MessageBox.Show("Please select a X or O from the Select Menu.");
                }
            }
            EndTurn();
            GameOver_Draw();
            GameOver_WinCheck();
            GameOver_Winner();
        } //done

        private void r1c2button_Click(object sender, EventArgs e)
        {
            if (r1c2button.Image == null)
            {
                if (blueToolStripMenuItem.Checked == true)
                {
                    //blue turn
                    if (xToolStripMenuItem.Checked == true)
                    {
                        //if blue user is x
                        r1c2button.Image = blueImages.Images[1];
                        r1c2button.Tag = 0;
                    }
                    else if (oToolStripMenuItem.Checked == true)
                    {
                        //if blue user is o
                        r1c2button.Image = blueImages.Images[0];
                        r1c2button.Tag = 0;
                    }
                    else
                    {
                        //msg box for issues
                        MessageBox.Show("Please select a X or O from the Select Menu.");
                    }
                }
                else if (redToolStripMenuItem.Checked == true)
                {
                    //red turn
                    if (xToolStripMenuItem.Checked == true)
                    {
                        //if user is x
                        r1c2button.Image = redImages.Images[1];
                        r1c2button.Tag = 1;
                    }
                    else if (oToolStripMenuItem.Checked == true)
                    {
                        //if user is o
                        r1c2button.Image = redImages.Images[0];
                        r1c2button.Tag = 1;
                    }
                    else
                    {
                        //msg box for issues
                        MessageBox.Show("Please select a X or O from the Select Menu.");
                    }

                }
                else
                {
                    //msg box for issues
                    MessageBox.Show("Please select a X or O from the Select Menu.");
                }
            }
            EndTurn();
            GameOver_Draw();
            GameOver_WinCheck();
            GameOver_Winner();
        } //done

        private void r1c3button_Click(object sender, EventArgs e)
        {
            if (r1c3button.Image == null)
            {
                if (blueToolStripMenuItem.Checked == true)
                {
                    //blue turn
                    if (xToolStripMenuItem.Checked == true)
                    {
                        //if blue user is x
                        r1c3button.Image = blueImages.Images[1];
                        r1c3button.Tag = 0;
                    }
                    else if (oToolStripMenuItem.Checked == true)
                    {
                        //if blue user is o
                        r1c3button.Image = blueImages.Images[0];
                        r1c3button.Tag = 0;
                    }
                    else
                    {
                        //msg box for issues
                        MessageBox.Show("Please select a X or O from the Select Menu.");
                    }
                }
                else if (redToolStripMenuItem.Checked == true)
                {
                    //red turn
                    if (xToolStripMenuItem.Checked == true)
                    {
                        //if user is x
                        r1c3button.Image = redImages.Images[1];
                        r1c3button.Tag = 1;
                    }
                    else if (oToolStripMenuItem.Checked == true)
                    {
                        //if user is o
                        r1c3button.Image = redImages.Images[0];
                        r1c3button.Tag = 1;
                    }
                    else
                    {
                        //msg box for issues
                        MessageBox.Show("Please select a X or O from the Select Menu.");
                    }

                }
                else
                {
                    //msg box for issues
                    MessageBox.Show("Please select a X or O from the Select Menu.");
                }
            }
            EndTurn();
            GameOver_Draw();
            GameOver_WinCheck();
            GameOver_Winner();
        } //done

        private void r2c1button_Click(object sender, EventArgs e)
        {
            if (r2c1button.Image == null)
            {
                if (blueToolStripMenuItem.Checked == true)
                {
                    //blue turn
                    if (xToolStripMenuItem.Checked == true)
                    {
                        //if blue user is x
                        r2c1button.Image = blueImages.Images[1];
                        r2c1button.Tag = 0;
                    }
                    else if (oToolStripMenuItem.Checked == true)
                    {
                        //if blue user is o
                        r2c1button.Image = blueImages.Images[0];
                        r2c1button.Tag = 0;
                    }
                    else
                    {
                        //msg box for issues
                        MessageBox.Show("Please select a X or O from the Select Menu.");
                    }
                }
                else if (redToolStripMenuItem.Checked == true)
                {
                    //red turn
                    if (xToolStripMenuItem.Checked == true)
                    {
                        //if user is x
                        r2c1button.Image = redImages.Images[1];
                        r2c1button.Tag = 1;
                    }
                    else if (oToolStripMenuItem.Checked == true)
                    {
                        //if user is o
                        r2c1button.Image = redImages.Images[0];
                        r2c1button.Tag = 1;
                    }
                    else
                    {
                        //msg box for issues
                        MessageBox.Show("Please select a X or O from the Select Menu.");
                    }

                }
                else
                {
                    //msg box for issues
                    MessageBox.Show("Please select a X or O from the Select Menu.");
                }
            }
            EndTurn();
            GameOver_Draw();
            GameOver_WinCheck();
            GameOver_Winner();
        } //done

        private void r2c2button_Click(object sender, EventArgs e)
        {
            if (r2c2button.Image == null)
            {
                if (blueToolStripMenuItem.Checked == true)
                {
                    //blue turn
                    if (xToolStripMenuItem.Checked == true)
                    {
                        //if blue user is x
                        r2c2button.Image = blueImages.Images[1];
                        r2c2button.Tag = 0;
                    }
                    else if (oToolStripMenuItem.Checked == true)
                    {
                        //if blue user is o
                        r2c2button.Image = blueImages.Images[0];
                        r2c2button.Tag = 0;
                    }
                    else
                    {
                        //msg box for issues
                        MessageBox.Show("Please select a X or O from the Select Menu.");
                    }
                }
                else if (redToolStripMenuItem.Checked == true)
                {
                    //red turn
                    if (xToolStripMenuItem.Checked == true)
                    {
                        //if user is x
                        r2c2button.Image = redImages.Images[1];
                        r2c2button.Tag = 1;
                    }
                    else if (oToolStripMenuItem.Checked == true)
                    {
                        //if user is o
                        r2c2button.Image = redImages.Images[0];
                        r2c2button.Tag = 1;
                    }
                    else
                    {
                        //msg box for issues
                        MessageBox.Show("Please select a X or O from the Select Menu.");
                    }

                }
                else
                {
                    //msg box for issues
                    MessageBox.Show("Please select a X or O from the Select Menu.");
                }
            }
            EndTurn();
            GameOver_Draw();
            GameOver_WinCheck();
            GameOver_Winner();
        } //done

        private void r2c3button_Click(object sender, EventArgs e)
        {
            if (r2c3button.Image == null)
            {
                if (blueToolStripMenuItem.Checked == true)
                {
                    //blue turn
                    if (xToolStripMenuItem.Checked == true)
                    {
                        //if blue user is x
                        r2c3button.Image = blueImages.Images[1];
                        r2c3button.Tag = 0;
                    }
                    else if (oToolStripMenuItem.Checked == true)
                    {
                        //if blue user is o
                        r2c3button.Image = blueImages.Images[0];
                        r2c3button.Tag = 0;
                    }
                    else
                    {
                        //msg box for issues
                        MessageBox.Show("Please select a X or O from the Select Menu.");
                    }
                }
                else if (redToolStripMenuItem.Checked == true)
                {
                    //red turn
                    if (xToolStripMenuItem.Checked == true)
                    {
                        //if user is x
                        r2c3button.Image = redImages.Images[1];
                        r2c3button.Tag = 1;
                    }
                    else if (oToolStripMenuItem.Checked == true)
                    {
                        //if user is o
                        r2c3button.Image = redImages.Images[0];
                        r2c3button.Tag = 1;
                    }
                    else
                    {
                        //msg box for issues
                        MessageBox.Show("Please select a X or O from the Select Menu.");
                    }

                }
                else
                {
                    //msg box for issues
                    MessageBox.Show("Please select a X or O from the Select Menu.");
                }
            }
            EndTurn();
            GameOver_Draw();
            GameOver_WinCheck();
            GameOver_Winner();
        } //done

        private void r3c1button_Click(object sender, EventArgs e)
        {
            if (r3c1button.Image == null)
            {
                if (blueToolStripMenuItem.Checked == true)
                {
                    //blue turn
                    if (xToolStripMenuItem.Checked == true)
                    {
                        //if blue user is x
                        r3c1button.Image = blueImages.Images[1];
                        r3c1button.Tag = 0;
                    }
                    else if (oToolStripMenuItem.Checked == true)
                    {
                        //if blue user is o
                        r3c1button.Image = blueImages.Images[0];
                        r3c1button.Tag = 0;
                    }
                    else
                    {
                        //msg box for issues
                        MessageBox.Show("Please select a X or O from the Select Menu.");
                    }
                }
                else if (redToolStripMenuItem.Checked == true)
                {
                    //red turn
                    if (xToolStripMenuItem.Checked == true)
                    {
                        //if user is x
                        r3c1button.Image = redImages.Images[1];
                        r3c1button.Tag = 1;
                    }
                    else if (oToolStripMenuItem.Checked == true)
                    {
                        //if user is o
                        r3c1button.Image = redImages.Images[0];
                        r3c1button.Tag = 1;
                    }
                    else
                    {
                        //msg box for issues
                        MessageBox.Show("Please select a X or O from the Select Menu.");
                    }

                }
                else
                {
                    //msg box for issues
                    MessageBox.Show("Please select a X or O from the Select Menu.");
                }
            }
            EndTurn();
            GameOver_Draw();
            GameOver_WinCheck();
            GameOver_Winner();
        } //done

        private void r3c2button_Click(object sender, EventArgs e)
        {
            if (r3c2button.Image == null)
            {
                if (blueToolStripMenuItem.Checked == true)
                {
                    //blue turn
                    if (xToolStripMenuItem.Checked == true)
                    {
                        //if blue user is x
                        r3c2button.Image = blueImages.Images[1];
                        r3c2button.Tag = 0;
                    }
                    else if (oToolStripMenuItem.Checked == true)
                    {
                        //if blue user is o
                        r3c2button.Image = blueImages.Images[0];
                        r3c2button.Tag = 0;
                    }
                    else
                    {
                        //msg box for issues
                        MessageBox.Show("Please select a X or O from the Select Menu.");
                    }
                }
                else if (redToolStripMenuItem.Checked == true)
                {
                    //red turn
                    if (xToolStripMenuItem.Checked == true)
                    {
                        //if user is x
                        r3c2button.Image = redImages.Images[1];
                        r3c2button.Tag = 1;
                    }
                    else if (oToolStripMenuItem.Checked == true)
                    {
                        //if user is o
                        r3c2button.Image = redImages.Images[0];
                        r3c2button.Tag = 1;
                    }
                    else
                    {
                        //msg box for issues
                        MessageBox.Show("Please select a X or O from the Select Menu.");
                    }

                }
                else
                {
                    //msg box for issues
                    MessageBox.Show("Please select a X or O from the Select Menu.");
                }
            }
            EndTurn();
            GameOver_Draw();
            GameOver_WinCheck();
            GameOver_Winner();
        } //done

        private void r3c3button_Click(object sender, EventArgs e)
        {
            if (r3c3button.Image == null)
            {
                if (blueToolStripMenuItem.Checked == true)
                {
                    //blue turn
                    if (xToolStripMenuItem.Checked == true)
                    {
                        //if blue user is x
                        r3c3button.Image = blueImages.Images[1];
                        r3c3button.Tag = 0;
                    }
                    else if (oToolStripMenuItem.Checked == true)
                    {
                        //if blue user is o
                        r3c3button.Image = blueImages.Images[0];
                        r3c3button.Tag = 0;
                    }
                    else
                    {
                        //msg box for issues
                        MessageBox.Show("Please select a X or O from the Select Menu.");
                    }
                }
                else if (redToolStripMenuItem.Checked == true)
                {
                    //red turn
                    if (xToolStripMenuItem.Checked == true)
                    {
                        //if user is x
                        r3c3button.Image = redImages.Images[1];
                        r3c3button.Tag = 1;
                    }
                    else if (oToolStripMenuItem.Checked == true)
                    {
                        //if user is o
                        r3c3button.Image = redImages.Images[0];
                        r3c3button.Tag = 1;
                    }
                    else
                    {
                        //msg box for issues
                        MessageBox.Show("Please select a X or O from the Select Menu.");
                    }

                }
                else
                {
                    //msg box for issues
                    MessageBox.Show("Please select a X or O from the Select Menu.");
                }
            }
            EndTurn();
            GameOver_Draw();
            GameOver_WinCheck();
            GameOver_Winner();
        } //done

        #endregion ButtonFunctionality


        //Game Mechanics for Tic-Tac-Toe including
        //user turns, and algorithm for win/loss
        #region GameMechanics
        private void EndTurn()
        {
            //change colors

            if (blueToolStripMenuItem.Checked == true) //if the users turn is blue
            {
                blueToolStripMenuItem.Checked = false;
                redToolStripMenuItem.Checked = true;
            }
            else //if the users turn is red
            {
                redToolStripMenuItem.Checked = false;
                blueToolStripMenuItem.Checked = true;
            }

            //change x/o

            if (xToolStripMenuItem.Checked == true) //if the user is X
            {
                xToolStripMenuItem.Checked = false;
                oToolStripMenuItem.Checked = true;
            }
            else //if the user is o
            {
                oToolStripMenuItem.Checked = false;
                xToolStripMenuItem.Checked = true;
            }
        } //done

        private void GameOver_Winner()
        {
            int winner = 0; //starting variable

            winner = Sequence(); //check for winner | 1 = player 1 winner | 2 = player 2 winner | 0 = no winner/board not full

            if (winner == 1)
            {
                MessageBox.Show("Player 1 Wins!");
                toolStripButton1_Click(this, new EventArgs());
            }
            else if (winner == 2)
            {
                MessageBox.Show("Player 2 Wins!");
                toolStripButton1_Click(this, new EventArgs());
            }
            else
            {
                //board is not finished and no winner
            }
        }

        private bool GameOver_WinCheck()
        {
            bool winner = false;
            //write sequence for gameover/win
            int playerWinner = 0;

            playerWinner = Sequence();

            if ((playerWinner == 1) || (playerWinner == 2))
            {
                winner = true;
            }
            else
            {
                winner = false;
            }
            return winner;
        }

        private void GameOver_Draw()
        {
            if ((FullBoard() == true) && (GameOver_WinCheck() == false)) //if the board is full and no moves can be made
            {
                MessageBox.Show("Draw!\nThere is no winner.");
                toolStripButton1_Click(this, new EventArgs());
            }
        } //game draw

        //button grid
        // r1c1 | r1c2 | r1c3
        // r2c1 | r2c2 | r2c3
        // r3c1 | r3c2 | r3c3


        #region AlgorithmSequences
        private int Sequence()
        {
            bool p1Winner = false; //player1winner
            bool p2Winner = false; //player2winner
            int winner = 0; //player?winner return variable

            // 0 = nothing
            // 1 = O
            // 2 = X

            //tmp variables
            int a = 0;
            int b = 0;
            int c = 0;
            int d = 0;
            int e = 0;
            int f = 0;
            int g = 0;
            int h = 0;
            int i = 0;
            
            //take the buttons and turn them into smaller "variables assigned with numbers
            //allowing the sequence function to then check for winners or none

            #region buttons
            //button 1
            if ((r1c1button.Tag.ToString() == "0"))
            {
                a = 1;
            }
            else if (r1c1button.Tag.ToString() == "1")
            {
                a = 2;
            }
            else
            {
                a = 0;
            }

            //button 2
            if (r1c2button.Tag.ToString() == "0")
            {
                b = 1;
            }
            else if (r1c2button.Tag.ToString() == "1")
            {
                b = 2;
            }
            else
            {
                b = 0;
            }

            //button 3
            if (r1c3button.Tag.ToString() == "0")
            {
                c = 1;
            }
            else if (r1c3button.Tag.ToString() == "1")
            {
                c = 2;
            }
            else
            {
                c = 0;
            }

            //button 4
            if (r2c1button.Tag.ToString() == "0")
            {
                d = 1;
            }
            else if (r2c1button.Tag.ToString() == "1")
            {
                d = 2;
            }
            else
            {
                d = 0;
            }

            //buttton 5
            if (r2c2button.Tag.ToString() == "0")
            {
                e = 1;
            }
            else if (r2c2button.Tag.ToString() == "1")
            {
                e = 2;
            }
            else
            {
                e = 0;
            }

            //button 6
            if (r2c3button.Tag.ToString() == "0")
            {
                f = 1;
            }
            else if (r2c3button.Tag.ToString() == "1")
            {
                f = 2;
            }
            else
            {
                f = 0;
            }

            //button 7
            if (r3c1button.Tag.ToString() == "0")
            {
                g = 1;
            }
            else if (r3c1button.Tag.ToString() == "1")
            {
                g = 2;
            }
            else
            {
                g = 0;
            }

            //button 8
            if (r3c2button.Tag.ToString() == "0")
            {
                h = 1;
            }
            else if (r3c2button.Tag.ToString() == "1")
            {
                h = 2;
            }
            else
            {
                h = 0;
            }

            //button 9
            if (r3c3button.Tag.ToString() == "0")
            {
                i = 1;
            }
            else if (r3c3button.Tag.ToString() == "1")
            {
                i = 2;
            }
            else
            {
                i = 0;
            }
            #endregion buttons

            for (int z = 0; z < 2; z++)
            {
                #region Sequence1
                if ((a == 1) && (b == 1) && (c == 1))
                {
                    p1Winner = true;
                }
                else if ((a == 2) && (b == 2) && (c == 2))
                {
                    p2Winner = true;
                }
                else
                {
                    winner = 0;
                }

                if ((a == 1) && (d == 1) && (g == 1))
                {
                    p1Winner = true;
                }
                else if ((a == 2) && (d == 2) && (g == 2))
                {
                    p2Winner = true;
                }
                else
                {
                    winner = 0;
                }

                if ((a == 1) && (e == 1) && (i == 1))
                {
                    p1Winner = true;
                }
                else if ((a == 1) && (e == 1) && (i == 1))
                {
                    p2Winner = true;
                }
                else
                {
                    winner = 0;
                }
                #endregion Sequence1

                #region Sequence2
                if ((b == 1) && (e == 1) && (h == 1))
                {
                    p1Winner = true;
                }
                else if ((b == 2) && (e == 2) && (h == 2))
                {
                    p2Winner = true;
                }
                else
                {
                    winner = 0;
                }

                #endregion Sequence2

                #region Sequence3
                if ((c == 1) && (e == 1) && (g == 1))
                {
                    p1Winner = true;
                }
                else if ((c == 2) && (e == 2) && (g == 2))
                {
                    p2Winner = true;
                }
                else
                {
                    winner = 0;
                }

                if ((c == 1) && (f == 1) && (i == 1))
                {
                    p1Winner = true;
                }
                else if ((c == 2) && (f == 2) && (i == 2))
                {
                    p2Winner = true;
                }
                else
                {
                    winner = 0;
                }

                #endregion Sequence3

                #region Sequence4
                if ((d == 1) && (e == 1) && (f == 1))
                {
                    p1Winner = true;
                }
                else if ((d == 2) && (e == 2) && (f == 2))
                {
                    p2Winner = true;
                }
                else
                {
                    winner = 0;
                }

                #endregion Sequence4


                // a | b | c
                // d | e | f
                // g | h | i

                if (p1Winner == true)
                {
                    winner = 1; //player 1 winner (O)
                }
                else if (p2Winner == true)
                {
                    winner = 2; //player 2 winner (X)
                }
                else
                {
                    winner = 0; //no winner
                }
            }
            return winner;
        }

        #endregion AlgorithmSequences

        private bool FullBoard()
        {
            bool fullBoard;
            //create tmp ints to calculate if the board if full or not
            int r1c1 = 0;
            int r1c2 = 0;
            int r1c3 = 0;
            int r2c1 = 0;
            int r2c2 = 0;
            int r2c3 = 0;
            int r3c1 = 0;
            int r3c2 = 0;
            int r3c3 = 0;

            if (r1c1button.Image != null)
            {
                r1c1 = 1;
            }
            if (r1c2button.Image != null)
            {
                r1c2 = 1;
            }
            if (r1c3button.Image != null)
            {
                r1c3 = 1;
            }
            if (r2c1button.Image != null)
            {
                r2c1 = 1;
            }
            if (r2c2button.Image != null)
            {
                r2c2 = 1;
            }
            if (r2c3button.Image != null)
            {
                r2c3 = 1;
            }
            if (r3c1button.Image != null)
            {
                r3c1 = 1;
            }
            if (r3c2button.Image != null)
            {
                r3c2 = 1;
            }
            if (r3c3button.Image != null)
            {
                r3c3 = 1;
            }

            if ((r1c1 == 1) && (r1c2 == 1) && (r1c3 == 1) && (r2c1 == 1) && (r2c2 == 1) && (r2c3 == 1) && (r3c1 == 1) && (r3c2 == 1) && (r3c3 == 1))
            {
                fullBoard = true;
            }
            else
            {
                fullBoard = false;
            }

            return fullBoard;
        } //check to see if board if full

        private bool LockPlayer()
        {
            bool newGame = true;
            bool currentGame = false;

            if (r1c1button.Image != null)
            {
                return currentGame;
            }
            else if (r1c2button.Image != null)
            {
                return currentGame;
            }
            else if (r1c3button.Image != null)
            {
                return currentGame;
            }
            else if (r2c1button.Image != null)
            {
                return currentGame;
            }
            else if (r2c2button.Image != null)
            {
                return currentGame;
            }
            else if (r2c3button.Image != null)
            {
                return currentGame;
            }
            else if (r3c1button.Image != null)
            {
                return currentGame;
            }
            else if (r3c2button.Image != null)
            {
                return currentGame;
            }
            else if (r3c3button.Image != null)
            {
                return currentGame;
            }
            else
            {
                return newGame;
            }
        } //check to see if the game has started or not


        #endregion GameMechanics


    }
}
